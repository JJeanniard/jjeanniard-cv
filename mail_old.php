<?php
header('Content-Type: application/json');

if(empty($_POST["name"])){
    die();
}else{
    $name = $_POST["name"];
}

if(empty($_POST["prenom"])){
    die();
}else{
    $prenom = $_POST["prenom"];
}

if(empty($_POST["email"])){
    die();
}else{
    $email = $_POST["email"];
}

if(empty($_POST["message"])){
    die();
}else{
    $message = $_POST["message"];
}
 
if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    echo "Email format invalid.";
    die();
}

$EmailTo = "jonathanjeanniard@sfr.fr";
$Subject = "New Message Received";
 
// prepare email body text
$Body = "Nom: ";
$Body .= $name;
$Body .= "\n";
 
$Body .= "Prenom: ";
$Body .= $prenom;
$Body .= "\n";

$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
 
$Body .= "Message: ";
$Body .= $message;
$Body .= "\n";
 
// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);


// redirect to success page
if ($success){
   $success = "success";
}else{
    $success = "error";
}