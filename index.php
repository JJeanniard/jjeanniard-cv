<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Curriculum Vitae Jeanniard Jonathan">
    <link rel="stylesheet" href="./public/css/style.css">
    <title>Jeanniard Jonathan Curriculum vitæ</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
        <section><h1></h1><p></p></section>
    </header>
    <main>
        <section><h2></h2></section>
        <section><h2></h2></section>
        <section><h2></h2></section>
    </main>
    <footer>
        <section><h2></h2></section>
        <section><h2></h2></section>
    </footer>
    <script src="js/app.js"></script>
</body>

</html>