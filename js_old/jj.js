/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
   
let scroll_pos = 0;
$('.bg-jj').css('background-color', 'rgba(255, 255, 255, 0.3)');
$('.bg-jj').css('box-shadow', '0 2px 5px 0 rgba(255,255,255,.16), 0 2px 10px 0 rgba(255,255,255,.12)');

$(document).scroll(function() { 
    scroll_pos = $(this).scrollTop();
    
    if(scroll_pos > 210) {
        $('.bg-jj').css('background-color', 'rgba(52, 58, 64, 1)');
        $('.bg-jj').css('box-shadow', '0 2px 5px 0 rgba(52,58,64,.16), 0 2px 10px 0 rgba(52,58,64,.12)');
    } else {
        $('.bg-jj').css('background-color', 'rgba(255, 255, 255, 0.3)');
        $('.bg-jj').css('box-shadow', '0 2px 5px 0 rgba(255,255,255,.16), 0 2px 10px 0 rgba(255,255,255,.12)');
    }
});

$('.carousel').carousel({
  interval: 1000000
});

$('.nav-link').on('click', function(){
    let clickedItem = $(this).attr('href');
   $('html, body').animate({
        scrollTop: $(clickedItem).offset().top - 50
   }, 1500);
});

$('#contactForm').on('submit', function (event){

    if (event.isDefaultPrevented()) {
        // handle the invalid form...

        submitMSG(false, "Did you fill in the form properly?");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    // Initiate Variables With Form Content
    let name = $("#inputname").val();
    let prenom = $("#inputprenom").val();
    let email = $("#inputemail").val();
    let message = $("#inputtext").val();
 
    $.ajax({
        type: "POST",
        url: "mail.php",
        data: "name=" + name + "&prenom=" + prenom + "&email=" + email + "&message=" + message,
        success : function(text){
            if (text == "success"){
                formSuccess();
                $(".modal-body").text(text);
            }
            else{
                formSuccess();
                $(".modal-body").text(text);
            }
        }
    });
}

function formSuccess(){
    $('#msg').modal({show:true});
}
